require 'rails_helper'

feature 'Following user profile' do
  let(:account_page) { Accounts::ShowPage.new }
  let(:home_page) { HomePage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account) }
  let(:actor) { create(:user, :with_account) }
  let!(:comment) { create(:comment, account: user.account) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(actor.email, 'TestPass')
  end

  scenario 'guest user tries to follow other user profile', js: true do
    account_page.load(username: user.account.username)

    expect(account_page).to be_displayed
    expect(account_page).to have_follow_button

    account_page.click_follow_button

    expect(account_page).to have_content 'You need to be signed in to do this'
    expect(account_page.follow_button).to have_content 'Follow'
    expect(user.account.followers.count).to eq 0
  end

  scenario 'signed-in user tries to follow other user profile', js: true do
    sign_user_in
    account_page.load(username: user.account.username)

    expect(account_page).to be_displayed
    expect(account_page).to have_follow_button

    account_page.click_follow_button

    expect(account_page.follow_button).to have_content 'Following'
    expect(user.account.followers.count).to eq 1
  end

  scenario 'signed-in user tries to unfollow other user profile', js: true do
    create(:follow, account: actor.account, target_account: user.account)

    sign_user_in
    account_page.load(username: user.account.username)

    expect(account_page).to be_displayed
    expect(account_page).to have_follow_button
    expect(account_page.follow_button).to have_content 'Following'

    account_page.click_follow_button

    expect(account_page.follow_button).to have_content 'Follow'
    expect(user.account.followers.count).to become 0
  end

  scenario 'signed-in user visits it\'s own profile' do
    sign_user_in
    account_page.load(username: actor.account.username)

    expect(account_page).to be_displayed
    expect(account_page).to_not have_follow_button
    expect(account_page).to have_edit_profile_button
  end
end
