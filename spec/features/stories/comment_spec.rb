require 'rails_helper'

feature 'Creating story' do
  let(:story_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }
  let!(:story) { create(:story) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  scenario 'signed in user wants to add first root comment for story', js: true do
    sign_user_in
    story_page.load(id: story.id)

    # Check if story page has all the commenting-related sections
    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_comments_tree
    expect(story_page.comments_tree).to have_root_comment_form
    expect(story_page.comments_tree.root_comment_form).to have_no_comments_yet
    expect(story_page.comments_tree.root_comment_form).to have_add_first_comment_btn

    # Click "Add first comment"
    story_page.comments_tree.root_comment_form.add_first_comment_btn.click
    sleep 2
    expect(story_page.comments_tree.root_comment_form).to have_new_comment_form
    new_comment_form = story_page.comments_tree.root_comment_form.new_comment_form

    # Try to create first root comment
    new_comment_form.add_comment(body: 'Test comment body')

    # Check if comment has been added and is visible
    story_page.comments_tree.wait_for_comments
    expect(story_page.comments_tree).to have_comments
    expect(story_page).to have_content 'Comment has been added'
  end

  scenario 'signed in user wants to add comment with empty body', js: true do
    create(:comment, story: story)

    sign_user_in
    story_page.load(id: story.id)

    # Check if story page has all the commenting-related sections
    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_comments_tree
    expect(story_page.comments_tree).to have_root_comment_form
    expect(story_page.comments_tree.root_comment_form).to have_new_comment_form
    new_comment_form = story_page.comments_tree.root_comment_form.new_comment_form

    # Send empty comment
    new_comment_form.add_comment(body: '')

    # Check if comment has not been added
    story_page.comments_tree.wait_for_comments
    expect(story_page.comments_tree.comments.length).to eq 1
    expect(Comment.all.count).to eq 1

    # Check if error message is present
    # expect(new_comment_form).to have_text 'Body can\'t be blank'
  end

  scenario 'guest user tries to add a root comment', js: true do
    create(:comment, story: story)
    story_page.load(id: story.id)

    # Check if story page has all the commenting-related sections
    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_comments_tree
    expect(story_page.comments_tree).to have_root_comment_form
    expect(story_page.comments_tree.root_comment_form).to have_new_comment_form
    new_comment_form = story_page.comments_tree.root_comment_form.new_comment_form

    expect(page).to have_content 'You need to be signed-in'
  end
end
