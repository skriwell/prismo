require 'rails_helper'

feature 'Creating story' do
  let(:new_story_page) { Stories::NewPage.new }
  let(:story_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }
  let(:home_page) { HomePage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }
  let!(:supergroup) { create(:group, :super) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  before do
    stub_request(:get, "https://example.com/")
      .to_return(status: 200, body: "", headers: {})
  end

  scenario 'user creates a fully valid story', js: true do
    sign_user_in
    home_page.load

    home_page.navbar.add_story_link.click
    sleep 2
    expect(new_story_page).to be_displayed

    new_story_page.url_field.set 'https://example.com'
    new_story_page.title_field.set 'Example title'
    new_story_page.set_tags %w[one two three four five]
    new_story_page.description_field.set 'Example description'
    expect(new_story_page).to have_submit_button
    new_story_page.submit_button.click

    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_content 'Story has been created'
    expect(story_page.stories.first.voted?).to eq true
  end

  scenario 'user creates story with empty URL', js: true do
    sign_user_in
    new_story_page.load

    new_story_page.url_field.set ''
    new_story_page.title_field.set 'Example title'
    new_story_page.set_tags %w[one two]
    new_story_page.submit_button.click

    expect(new_story_page).to be_displayed
    expect(new_story_page).to have_content 'Url is required when description is empty'
  end

  scenario 'user creates story with URL taken', js: true do
    story = create(:story, :link)

    sign_user_in
    new_story_page.load

    new_story_page.url_field.set story.url
    new_story_page.title_field.set 'Example title'
    new_story_page.set_tags %w[one two]
    new_story_page.description_field.set 'Example description'
    new_story_page.submit_button.click

    expect(new_story_page).to be_displayed
    expect(new_story_page).to have_content 'Url has already been taken'
  end

  scenario 'user creates a story with wrong number of tags', js: true do
    sign_user_in
    home_page.load

    home_page.navbar.add_story_link.click
    sleep 2
    expect(new_story_page).to be_displayed

    # Min tags
    Setting.min_story_tags = 2
    new_story_page.set_tags %w[one]
    new_story_page.submit_button.click

    expect(new_story_page).to be_displayed
    expect(new_story_page).to have_content('Tag list needs to have at least 2 tags selected')
  end
end
