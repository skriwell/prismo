require 'rails_helper'

feature 'Creating sub-comment' do
  let(:story_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }
  let!(:story) { create(:story) }
  let!(:comment) { create(:comment, story: story) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  scenario 'signed in user wants to add sub-comment', js: true do
    sign_user_in
    story_page.load(id: story.id)

    # Check if story page has all the commenting-related sections
    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_comments_tree
    expect(story_page.comments_tree).to have_root_comment_form

    expect(story_page.comments_tree).to have_comments
    first_comment = story_page.comments_tree.comments.first

    first_comment.reply_btn.click
    first_comment.wait_for_new_comment_form
    expect(first_comment).to have_new_comment_form
    new_comment_form = first_comment.new_comment_form

    new_comment_form.add_comment(body: 'Example sub-comment body')

    # Check if comment has been added and is visible
    first_comment.wait_for_child_comments
    expect(first_comment).to have_child_comments
    expect(story_page).to have_content 'Comment has been added'
  end

  scenario 'guest user wants to add sub-comment', js: true do
    story_page.load(id: story.id)

    # Check if story page has all the commenting-related sections
    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_comments_tree
    expect(story_page.comments_tree).to have_root_comment_form

    expect(story_page.comments_tree).to have_comments
    first_comment = story_page.comments_tree.comments.first

    first_comment.reply_btn.click
    first_comment.wait_for_new_comment_form

    expect(first_comment).to have_new_comment_form
    expect(first_comment).to have_content 'You need to be signed-in'
  end
end
