require 'rails_helper'

describe ActivityPub::UpdateDistributionJob do
  let(:instance) { described_class.new }

  let(:account)  { create(:account) }
  let(:follower) { create(:account, inbox_url: 'http://example.com') }

  describe '#perform' do
    before do
      allow(ActivityPub::DeliveryJob).to receive(:perform_later)
      follower.follow!(account)
    end

    context 'when resource is Account' do
      it 'delivers to followers' do
        instance.perform(account.id, 'Account')

        expect(ActivityPub::DeliveryJob)
          .to have_received(:perform_later)
          .with(any_args, 'http://example.com')
      end
    end

    context 'when resource is Story' do
      let(:story) { create(:story, account: account) }

      it 'delivers to followers' do
        instance.perform(story.id, 'Story')

        expect(ActivityPub::DeliveryJob)
          .to have_received(:perform_later)
          .with(any_args, 'http://example.com')
      end
    end

    context 'when resource is Comment' do
      let(:comment) { create(:comment, account: account) }

      it 'delivers to followers' do
        instance.perform(comment.id, 'Comment')

        expect(ActivityPub::DeliveryJob)
          .to have_received(:perform_later)
          .with(any_args, 'http://example.com')
      end
    end
  end
end
