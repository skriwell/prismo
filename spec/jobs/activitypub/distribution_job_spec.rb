require 'rails_helper'

describe ActivityPub::DistributionJob do
  subject { described_class.new }

  let(:remote_account) { create(:account, :remote, inbox_url: 'https://example.com') }
  let!(:story) { create(:story) }
  let!(:remote_comment) { create(:comment, :remote, account: remote_account, story: story) }
  let!(:local_comment) { create(:comment, story: story) }

  before do
    stub_jsonld_contexts!
  end

  describe '#perform' do
    before do
      allow(ActivityPub::DeliveryJob).to receive(:perform_later)
    end

    it 'delivers to followers' do
      subject.perform('Comment', local_comment.id)

      expect(ActivityPub::DeliveryJob)
        .to have_received(:perform_later)
        .with(any_args, local_comment.account_id, 'https://example.com')
    end
  end
end
