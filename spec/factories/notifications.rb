FactoryBot.define do
  factory :notification do
    event_key 'comment_reply'
    seen false

    association :author, factory: :account
    association :recipient, factory: :account
    association :notifable, factory: :comment
    association :context, factory: :story
  end
end
