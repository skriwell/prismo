require "rails_helper"

describe StoryPolicy do
  subject { described_class }

  let(:user) { create(:user, :with_account) }
  let(:admin) { create(:user, :admin, :with_account) }
  let(:account) { user.account }
  let(:story) { create(:story, account: account) }

  shared_examples 'is prohibited for guests' do
    it { expect(subject).to_not permit(nil, story) }
  end

  shared_examples 'is available for guests' do
    it { expect(subject).to permit(nil, story) }
  end

  shared_examples 'is available for any non-guest user' do
    it { expect(subject).to permit(create(:user, :with_account), story) }
  end

  shared_examples 'is prohibitet for user other than story author' do
    it { expect(subject).not_to permit(create(:account).user, story) }
  end

  shared_examples 'is available for story author' do
    it { expect(subject).to permit(story.account.user, story) }
  end

  shared_examples 'is available for admin' do
    it { expect(subject).to permit(admin, story) }
  end

  permissions :index? do
    it_behaves_like 'is available for guests'
    it_behaves_like 'is available for any non-guest user'
  end

  permissions :create? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
  end

  permissions :update? do
    it_behaves_like 'is prohibitet for user other than story author'
    it_behaves_like 'is available for story author'
    it_behaves_like 'is available for admin'
  end

  permissions :scrap? do
    it_behaves_like 'is prohibitet for user other than story author'
    it_behaves_like 'is available for story author'
    it_behaves_like 'is available for admin'
  end

  permissions :comment? do
    it_behaves_like 'is prohibited for guests'
    it_behaves_like 'is available for any non-guest user'
  end
end
