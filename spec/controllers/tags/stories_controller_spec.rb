require 'rails_helper'

describe Tags::StoriesController, type: :controller do
  render_views

  let(:tag) { create(:tag) }

  describe 'GET #hot' do
    subject { get :hot, params: { tag_name: tag.name }, format: format }

    context 'when requesting html' do
      let(:format) { :html }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'text/html'
      end
    end

    context 'when requesting atom' do
      let(:format) { :atom }

      it 'renders successfull response' do
        subject
        expect(response).to be_successful
        expect(response.content_type).to eq 'application/atom+xml'
      end
    end
  end

  describe 'GET #recent' do
    subject { get :recent, params: { tag_name: tag.name } }

    it 'renders successfull response' do
      subject
      expect(response).to be_successful
      expect(response.content_type).to eq 'text/html'
    end
  end
end
