require 'rails_helper'

describe Auth::RegistrationsController, type: :controller do
  render_views

  shared_examples 'checks for enabled registrations' do |path|
    around do |example|
      open_registrations = Setting.open_registrations
      example.run
      Setting.open_registrations = open_registrations
    end

    it 'redirects if it is not open for registration' do
      Setting.open_registrations = false

      post path

      expect(response).to redirect_to '/'
    end
  end

  describe 'GET #new' do
    before do
      request.env['devise.mapping'] = Devise.mappings[:user]
    end

    context do
      around do |example|
        open_registrations = Setting.open_registrations
        example.run
        Setting.open_registrations = open_registrations
      end

      it 'returns http success' do
        Setting.open_registrations = true
        get :new
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'POST #create' do
    before { request.env["devise.mapping"] = Devise.mappings[:user] }

    context do
      around do |example|
        open_registrations = Setting.open_registrations
        example.run
        Setting.open_registrations = open_registrations
      end

      subject do
        Setting.open_registrations = true
        post :create, params: { user: { account_attributes: { username: 'test' }, email: 'test@example.com', password: '12345678', password_confirmation: '12345678' } }
      end

      it 'redirects to login page' do
        subject
        expect(response).to redirect_to root_path
      end

      it 'creates user' do
        subject
        user = User.find_by(email: 'test@example.com')
        expect(user).to_not be_nil
      end
    end

    it 'does nothing if user already exists' do
      create(:user, account: create(:account, username: 'test'))
      subject
    end

    include_examples 'checks for enabled registrations', :create
  end
end
