RSpec::Matchers.define :become do |expected|
  match do |block|
    begin
      Timeout.timeout(Capybara.default_max_wait_time) do
        sleep(0.1) until value = expected
        value
      end
    rescue TimeoutError
      false
    end
  end
end
