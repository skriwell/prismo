class HomePage < SitePrism::Page
  set_url '/'
  set_url_matcher %r{\/\z}

  section :navbar, ::NavbarSection, '.navbar-main'
  sections :stories, ::StoryRowSection, '.stories-list li.story-row'
end
