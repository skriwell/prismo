require 'rails_helper'

describe BodyParser do
  let(:service) { described_class.new(body) }

  describe '#call' do
    subject { service.call }

    context 'when body contains script tag' do
      let(:body) { '<script>alert("yo")</script> <a href="https://example.com">yo</a>' }

      it 'strips the script tag' do
        expect(subject).to eq "<p>alert(&quot;yo&quot;) <a href=\"https://example.com\">yo</a></p>\n"
      end
    end

    context 'when body contains markdown marks' do
      let(:body) { '**bold** not bold' }

      it 'converts them to proper HTML tags' do
        expect(subject).to eq "<p><strong>bold</strong> not bold</p>\n"
      end
    end

    context 'when body contains URL' do
      let(:body) { 'Is https://example.com linked properly?' }

      it 'converts URL to link' do
        expect(subject).to eq "<p>Is <a href=\"https://example.com\">https://example.com</a> linked properly?</p>\n"
      end
    end

    context 'when body contains a markdown link' do
      let(:body) { 'Is [this link](https://example.com) linked properly?' }

      it 'converts URL to link properly' do
        expect(subject).to eq "<p>Is <a href=\"https://example.com\">this link</a> linked properly?</p>\n"
      end
    end

    context 'when body contains a local mention' do
      let(:body) { "Hey there @mxb what's up?" }

      it 'converts mention to link' do
        expect(subject).to eq "<p>Hey there <a href=\"/@mxb\">@mxb</a> what&#39;s up?</p>\n"
      end
    end

    context 'when body contains a remote mention' do
      let(:body) { "Hey there @mb@mstdn.io what's up?" }

      it 'converts mention to link' do
        expect(subject).to eq "<p>Hey there <a href=\"https://mstdn.io/@mb\">@mb</a> what&#39;s up?</p>\n"
      end
    end

    context 'when body contains a mail' do
      let(:body) { 'Hey my mail is mb@mstdn.io' }

      it 'leaves email address in raw form' do
        expect(subject).to eq "<p>Hey my mail is mb@mstdn.io</p>\n"
      end
    end
  end
end
