class AddUriToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :uri, :string
  end
end
