class RemoveGroupStories < ActiveRecord::Migration[5.2]
  def change
    drop_table :group_stories
  end
end
