class AddUrlAndDomainToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :url, :string
    add_column :comments, :domain, :string
  end
end
