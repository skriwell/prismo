class StoriesController < ApplicationController
  layout 'application'

  before_action :authenticate_user!, only: %i[new create edit update]
  before_action :set_account_upvoted_story_ids

  def index
    @page_title = 'Hot stories'
    @feed_title = @page_title
    @stories = StoriesQuery.new.hot.page(params[:page])

    respond_to do |format|
      format.html { render :index }
      format.atom { render :index }
    end
  end

  def recent
    @page_title = 'Recent stories'
    @feed_title = @page_title
    @stories = StoriesQuery.new.recent.page(params[:page])

    respond_to do |format|
      format.html { render :index }
      format.atom { render :index }
    end
  end

  def show
    set_account_upvoted_comment_ids

    @story = Story.find(params[:id])
    @comments = @story.comments.includes(:account).hash_tree
    @comment = Comments::Create.new(
      story_id: @story.id
    )

    respond_to do |format|
      format.html
      format.json do
        render json: ActivityPub::StorySerializer.new(@story),
               with_context: true,
               content_type: 'application/activity+json'
      end
    end
  end

  def new
    @story = Stories::Create.new(*new_story_params)
    authorize @story
  end

  def create
    authorize Stories::Create

    outcome = Stories::Create.run(
      title: params.fetch(:story)[:title],
      url: params.fetch(:story)[:url],
      description: params.fetch(:story)[:description],
      tag_list: params.fetch(:story)[:tag_list],
      account: current_account
    )

    if outcome.valid?
      redirect_to outcome.result, notice: 'Story has been created'
    else
      @story = outcome
      render :new
    end
  end

  def edit
    story = find_story
    authorize story

    @story = Stories::Update.new(
      story: story,
      url: story.url,
      title: story.title,
      tag_list: story.tag_names.join(', '),
      description: story.description,
    )
  end

  def update
    story = find_story
    authorize story

    outcome = Stories::Update.run(
      story: story,
      url: params.fetch(:story)[:url],
      title: params.fetch(:story)[:title],
      tag_list: params.fetch(:story)[:tag_list],
      description: params.fetch(:story)[:description],
      account: current_account
    )

    if outcome.valid?
      redirect_to outcome.result, notice: 'Story has been updated'
    else
      @story = outcome
      render :edit
    end
  end

  private

  def new_story_params
    params.permit(:url, :title)
  end

  def find_story
    Story.find(params[:id])
  end
end
