class Dashboard::FollowedAccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_account_upvoted_story_ids

  before_action { set_jumpbox_link(Jumpbox::FOLLOWED_USERS_LINK) }

  def show
    @page_title = 'Hot stories'

    stories = StoriesQuery.new.hot
    stories = StoriesQuery.new(stories).by_followed_accounts(current_account)

    @stories = stories.page(params[:page])
  end

  def recent
    @page_title = 'Recent stories'

    stories = StoriesQuery.new.recent
    stories = StoriesQuery.new(stories).by_followed_accounts(current_account)

    @stories = stories.page(params[:page])

    render :show
  end
end
