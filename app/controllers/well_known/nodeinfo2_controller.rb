module WellKnown
  class Nodeinfo2Controller < ActionController::Base
    def show
      render json: Nodeinfo2Serializer.new
    end
  end
end
