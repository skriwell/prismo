class ApplicationController < ActionController::Base
  include Pundit
  include HasJumpbox

  helper_method :current_account

  before_action :detect_device_format

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def current_account
    current_user&.account&.decorate
  end

  def set_account_upvoted_comment_ids
    ids = user_signed_in? ? current_account.upvoted_comments.map(&:voteable_id) : []
    @account_upvoted_comment_ids = ids
  end

  def set_account_upvoted_story_ids
    ids = user_signed_in? ? current_account.upvoted_stories.map(&:voteable_id) : []
    @account_upvoted_story_ids = ids
  end

  def detect_device_format
    case request.user_agent
    when /iPhone/i
      request.variant = :phone
    when /Android/i && /mobile/i
      request.variant = :phone
    when /Windows Phone/i
      request.variant = :phone
      end
  end

  def user_not_authorized
    flash[:alert] = I18n.t('generic.not_authorized')
    redirect_to(request.referrer || root_path)
  end
end
