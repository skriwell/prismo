class Tags::StoriesController < ApplicationController
  layout 'application'

  before_action :set_account_upvoted_story_ids

  before_action { set_jumpbox_link(find_tag) }

  def hot
    @tag = find_tag
    stories = StoriesQuery.new.hot
    stories = StoriesQuery.new(stories).tagged_with([@tag.name])

    @feed_title = "Hot stories in #{@tag.decorate}"
    set_meta_tags @tag.decorate.to_meta_tags.merge(title: @feed_title)

    @stories = stories.page(params[:page])

    respond_to do |format|
      format.html { render :index }
      format.atom { render 'stories/index' }
    end
  end

  def recent
    @tag = find_tag
    stories = StoriesQuery.new.recent
    stories = StoriesQuery.new(stories).tagged_with([@tag.name])

    @feed_title = "Recent stories in #{@tag.decorate}"
    set_meta_tags @tag.decorate.to_meta_tags.merge(title: @feed_title)

    @stories = stories.page(params[:page])

    respond_to do |format|
      format.html { render :index }
      format.atom { render :index }
    end
  end

  private

  def find_tag
    @find_tag ||= Gutentag::Tag.find_by!(name: params[:tag_name]).decorate
  end
end
