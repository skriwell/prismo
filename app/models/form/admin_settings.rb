class Form::AdminSettings
  include ActiveModel::Model

  delegate(
    :site_title,
    :site_title=,
    :site_description,
    :site_description=,
    :open_registrations,
    :open_registrations=,
    :closed_registrations_message,
    :closed_registrations_message=,
    :stories_per_day,
    :stories_per_day=,
    :story_votes_per_day,
    :story_votes_per_day=,
    :comment_votes_per_day,
    :comment_votes_per_day=,
    to: Setting
  )
end
