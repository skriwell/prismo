class Comment < ApplicationRecord
  has_closure_tree

  HOT_DAYS_LIMIT = 40

  belongs_to :story, counter_cache: true
  belongs_to :account, counter_cache: true, optional: true
  has_many :votes, as: :voteable

  scope :local, -> { where(uri: nil) }
  scope :remote, -> { where.not(uri: nil) }

  def cache_depth
    update_attributes(depth_cached: depth)
  end

  def cache_body
    update_attributes(
      body_html: BodyParser.new(body).call
    )
  end

  def object_type
    :note
  end

  def self.find_local(id)
    find_by(id: id, domain: nil)
  end
end
