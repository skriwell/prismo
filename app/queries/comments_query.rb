class CommentsQuery
  attr_reader :relation

  def initialize(relation = Comment.all)
    @relation = relation
  end

  def with_includes
    relation.includes(:story, :account)
  end

  def with_story
    relation.includes(:story)
  end

  def hot
    with_includes.order(Arel.sql('ranking(votes_count, created_at::timestamp, 3) DESC'))
                 .where('created_at > ?', Comment::HOT_DAYS_LIMIT.days.ago)
  end

  def recent
    with_includes.order(created_at: :desc)
  end

  def by_account(account)
    with_includes.where(account_id: account.id)
  end
end
