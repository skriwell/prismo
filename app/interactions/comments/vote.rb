class Comments::Vote < ActiveInteraction::Base
  object :comment
  object :account

  def to_model
    ::Vote.new
  end

  def execute
    return existing_vote if existing_vote.present?

    vote = ::Vote.new(voteable: comment)
    vote.account = account

    if vote.save
      account.touch(:last_active_at)
      Accounts::UpdateKarmaJob.perform_later(comment.account.id, 'Comment')
      ActivityPub::VoteDistributionJob.perform_later(vote.id) if !comment.account.local?
    else
      errors.merge!(vote.errors)
    end

    vote
  end

  private

  def existing_vote
    @existing_vote ||= ::Vote.find_by(voteable: comment, account: account)
  end
end
