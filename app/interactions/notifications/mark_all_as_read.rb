class Notifications::MarkAllAsRead < ActiveInteraction::Base
  object :account

  def execute
    return if account.notifications_as_recipient.not_seen.empty?

    account.notifications_as_recipient.not_seen.update_all(seen: true)

    NotificationsChannel.broadcast_to(
      account.user,
      event: 'notifications.marked_all_as_read'
    )
  end
end
