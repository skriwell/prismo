class AccountPolicy < ApplicationPolicy
  def toggle_follow?
    user.present?
  end
end
