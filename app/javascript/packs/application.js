import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import Turbolinks from "turbolinks"
import axios from 'axios'
import Rails from 'rails-ujs'
import errorStatusResolver from './lib/error_status_resolver'

if (process.env.SENTRY_DSN_FE) {
  Raven.config(process.env.SENTRY_DSN_FE).install()
}

// Load tailwind setup
import '../css/application_tailwind.css'

// Load spectre.css
import 'feather-icon-font/dist/feather'
import '../css/application.scss'

// Tippy.js
import 'css/vendor/tippy/tippy'

// Init rails UJS
Rails.start()

// Init turbolinks
Turbolinks.start()

// Init axios
const csrfEl = document.querySelector("meta[name=csrf-token]")
const csrfToken = csrfEl ? csrfEl.content : null
axios.defaults.headers.common['X-CSRF-Token'] = csrfToken

axios.interceptors.response.use((response) => {
  return response;
}, (error) => {
  errorStatusResolver(error.response.status)
  return Promise.reject(error)
})

// Init app
const application = Application.start()
const context = require.context("./controllers", true, /\.js$/)
application.load(definitionsFromContext(context))
