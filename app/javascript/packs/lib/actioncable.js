import ActionCable from 'actioncable'

let https = process.env.RAILS_ENV == 'production' || process.env.LOCAL_HTTPS == 'true'
let protocol = https ? 'wss' : 'ws'

const ws = ActionCable.createConsumer(`${protocol}://${process.env.LOCAL_DOMAIN}/cable`)

export default ws
