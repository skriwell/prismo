import BaseController from "./base_controller"
import tippy from 'tippy.js'
import { timingSafeEqual } from "crypto";

export default class extends BaseController {
  static targets = ['toggle', 'dropdown']

  connect() {
    tippy(this.toggleTarget, {
      content: this.dropdownTarget.outerHTML,
      interactive: true,
      performance: true,
      placement: this.placement,
      theme: this.theme,
      trigger: this.trigger,
      arrow: this.arrow
    })
  }

  disconnect() {
    this.toggleTarget._tippy.destroy()
  }

  get placement () {
    return this.data.get('placement') || 'top'
  }

  get theme () {
    return this.data.get('theme') || 'prismo-dropdown'
  }

  get trigger () {
    return this.data.get('trigger') || 'mouseenter focus'
  }

  get arrow () {
    return this.data.get('arrow') == 'true'
  }
}
