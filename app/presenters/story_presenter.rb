class StoryPresenter
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::TextHelper

  attr_reader :story

  def initialize(story)
    @story = story
  end

  def excerpt
    if story.url_meta&.description
      story.url_meta.description.truncate(100)
    else
      story.decorate.description_excerpt
    end
  end

  def tags
    story.tags.to_a.map(&:decorate)
  end

  def primary_tags
    tags.shift(2)
  end

  def more_tags
    more_tags_seleciton = tags - primary_tags
    more_tags_seleciton.any? ? more_tags_seleciton : []
  end

  def thumb_classes
    classes = []
    return if story.thumb.present?

    classes.push('story-row-thumb-empty')
    classes.push('story-row-thumb-empty--article') if story.article?

    classes
  end

  def thumb_styles
    return nil if story.thumb.blank?

    "background-image: url(#{story.thumb_url(:size_200)})"
  end
end
